# JsonWebToken Guard & Middleware for NestJS

Depends on `@nestling/context`, `@nestling/errors` and `@nestling/config`

Used to get decoded json webtoken information into the context.

Things like revoked tokens are expected to be handled by an api gateway.

## Config

```typescript
{
  jwt: {
    secret: 'some_secret',
    header?: 'authorization',
    contextKey?: 'jwt'
  } 
}
```

## Guard Usage

```typescript
@UseGuards(JsonWebtokenGuard)
```

## Middleware Usage 

```typescript
export class UserModule {
  configure(consumer: MiddlewaresConsumer) {
    consumer.apply(JsonWebTokenMiddleware).forRoutes(
      { path: '/user/list', method: RequestMethod.GET },
      { path: '/user', method: RequestMethod.PUT },
      { path: '/user/:_id', method: RequestMethod.GET },
      { path: '/user/password', method: RequestMethod.POST }
    )
  }
}
```
