import { Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response, NextFunction } from 'express'
import * as jwt from 'jsonwebtoken'
import { authErrors } from './errors'
import { ConfigService } from '@nestling/config'
import { setContext } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'

export type JWTConfig = {
  jwt: {
    contextKey?: string
    secret: string
    header?: string
  }
}

@Injectable()
export class JsonWebTokenMiddleware implements NestMiddleware {
  constructor (
    private config: ConfigService
  ) {

  }
  use (
      request: Request,
      response: Response,
      next: NextFunction
    ) {
    const {
      jwt: {
        contextKey = 'jwt',
        secret,
        header = 'authorization'
      }
    }: JWTConfig = this.config as any

    if (
      request.headers[header] &&
      (request.headers[header] as string).split(' ')[0] === 'Bearer'
    ) {
      const token = (request.headers[header] as string).split(' ')[1]
      const decoded: any = jwt.verify(token, secret)

      setContext(contextKey, decoded, request)

      next()
    } else {
      throw new ErrorMessage('auth:unauthorized')
    }
  }
}

ErrorMessage.addErrorMessages(authErrors)
